# Wordpress Starter

This is a Wordpress Starter Setup for development of Wordpress themes and plugins utilizing modern web development practices.

## Pre-Requisites

###PHP Version

In order to follow some of the modern web development standards, such as PHP Namespaces, this Starter relies heavly on features implemented in PHP > 5.3

Will be adding some 5.4 features such as Traits etc, but will fork this.

### Sub-Modules

This repository uses submodules for the following:

Wordpress Core : [https://github.com/WordPress/WordPress.git](https://github.com/WordPress/WordPress.git)


**To use these submodules use the following git commands:**

`git submodule init`

`git submodule update`