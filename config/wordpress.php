<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */


define('APP_ROOT', dirname(__DIR__));

if(file_exists(APP_ROOT.'/config/env/local.php')) {
	require APP_ROOT.'/config/env/local.php';
} else {
	switch($_SERVER['SERVER_NAME']) {
        case "wordpress-starter.dev":
		case "localhost":
		case "development":
			require APP_ROOT . '/config/env/development.php';
			break;
		case "staging":
			require APP_ROOT . '/config/env/staging.php';
			break;
		case "live":
		case "production":
			require APP_ROOT . '/config/env/production.php';
			break;
	}
	
}

define('WP_HOME', 'http://wordpress-starter.dev');
define('WP_SITEURL', WP_HOME.'/site/');

define('WP_CONTENT_DIR', APP_ROOT.'/public_html/content');
define('WP_CONTENT_URL', WP_HOME.'/content');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
